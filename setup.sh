yarn add next react react-dom
touch tsconfig.json
yarn add --dev typescript @types/react @types/node
yarn add node-sass — dev
touch pages/_document.tsx
echo "import Document, {Html, Head, Main, NextScript} from 'next/document'

export default class CustomDocument extends Document{
    render(){
        return <Html>
            <Head>
                <meta property=\"custom\" content=\"null\"/>
            </Head>
            <body>
            <Main/>
            </body>
            <NextScript/>
        </Html>
    }
}" >> pages/_document.tsx
mkdir components
mkdir constants
mkdir contexts
mkdir providers
mkdir redux
mkdir redux/actions
mkdir redux/reducers
mkdir redux/store
mkdir redux/types
mkdir shared
mkdir shared/jsons
mkdir shared/libs
mkdir shared/styles
mkdir widgets
yarn add @reduxjs/toolkit
yarn add redux
yarn add react-redux
yarn add @types/uuid
yarn add redux-logger
yarn add @types/react-redux
yarn add @types/redux-logger
yarn add aws-amplify @aws-amplify/ui-react @emotion/css
amplify init
#amplify add auth
amplify push --y
yarn build

