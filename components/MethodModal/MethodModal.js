import React from 'react'
import styles from './MethodModal.module.css'
export default function MethodModal(props) {

const modalstyles = {
    width: '200px'
}

const containerstyles = {
    position: 'absolute',
    left: '6%',
    top: '-5%'
}

    return (
    props.visibility 
    ? <span style={containerstyles}>
        <img style={modalstyles} src='circle.png'></img>
        <span className={styles.centered}>{props.text}</span>
    </span> 
    : <span></span>
    )
}
