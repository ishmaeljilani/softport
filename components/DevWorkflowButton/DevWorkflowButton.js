import React , {useState} from 'react'
import styles from './DevWorkflowButton.module.css'
import MethodModal from '../MethodModal/MethodModal.js'
import { container } from 'aws-amplify';

function DevWorkflowButton(props) {

    const [visible, setVisbility] = useState(false);

    return (
        <>
        <div className={styles.button}>
            <img onClick={()=> setVisbility(!visible)} className={styles.click} src={props.image}>
            </img>
        </div>
        <span className={styles.overlay}>
            <MethodModal visibility={visible} text={props.first}>
            </MethodModal>
        </span>
        <span className={styles.overlay}>
            <MethodModal visibility={visible} text={props.second}>
            </MethodModal>
        </span>
        </>
    )
}

export default DevWorkflowButton
