import { create } from "react-test-renderer";
import DevWorkflowButton from './DevWorkflowButton.js'
describe("Button component", () => {
  test("Matches the snapshot", () => {
    const button = create(<DevWorkflowButton />);
    expect(button.toJSON()).toMatchSnapshot();
  });
});
