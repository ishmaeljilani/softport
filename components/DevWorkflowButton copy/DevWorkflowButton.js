import React , {useState} from 'react'
import styles from './DevWorkflowButton.module.css'

function DevWorkflowButton(props) {

    const [visible, setVisbility] = useState(false);

    return (
        <>
        <div className={styles.button}>
            <a href={props.link}><img onClick={()=> setVisbility(!visible)} className={styles.click} src={props.image}>
            </img></a>
        </div>
        
        </>
    )
}

export default DevWorkflowButton
