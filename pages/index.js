import Head from 'next/head'
import styles from '../styles/gridindex.module.css'
import stylesdesk from '../styles/griddesktop.module.css'
import WorkflowButton from '../components/DevWorkflowButton copy/DevWorkflowButton'

export default function Home() {
  return (
    <>
      <Head>
        <title>IJ Software Portfolio</title>
        <link rel="icon" href="/react.svg" />
      </Head>
      <div className={styles.smallwrapper}>
      <div className={styles.grid_container}>
      <div className={styles.top}>
      <div className={styles.myname}>Ishmael Jilani</div>
      <div className={styles.softend}>Software Engineer</div>
      <div className={styles.slogan}>Specify. Test. Engineer. Automate.</div>
      <div className={styles.icons}>
        <div className={styles._1}></div>
        <div className={styles._2}></div>
        <div className={styles._3}></div>
        <div className={styles._4}></div>
        <div className={styles._5}></div>
      </div>
      <div className={styles.nextjs}></div>
      <div className={styles.amplify}></div>
    </div>
    <div className={styles.bottom}>
      <div className={styles._a}><WorkflowButton link='/UI.pdf' image="design.png"></WorkflowButton></div>
      <div className={styles._b}><WorkflowButton link='/UX.pdf' image="dev.png"></WorkflowButton></div>
      <div className={styles._c}><WorkflowButton link='/FrontEnd.pdf' image="deploy.png"></WorkflowButton></div>
      <div className={styles.footer}></div>
    </div>
  </div>
  </div>
  <div className={styles.desktopcontainer}>
  <div className={stylesdesk.grid_container}>
    <div className={stylesdesk.top}>
  <div className={stylesdesk.figma}><img src="figma.png" /></div>
  <div className={stylesdesk.react}><img src="react.png" /></div>
  <div className={stylesdesk.next}><img src="next.png" /></div>
  <div className={stylesdesk.nodejs}><img src="nodejs.png" /></div>
  <div className={stylesdesk.express}><img src="express.png" /></div>
  <div className={stylesdesk.rest}><img src="rest.png" /></div>
  <div className={stylesdesk.graph}><img src="graphql.png" /></div>
  <div className={stylesdesk.apollo}><img src="apollo.png" /></div>
  <div className={stylesdesk.postgres}><img src="postgres.png" /></div>
  <div className={stylesdesk.mongodb}><img src="mongodb.png" /></div>
  <div className={stylesdesk.amplify}><img src="amplify.png" /></div>
  <div className={stylesdesk.name}>Ishmael Jilani</div>
  <div className={stylesdesk.softeng}>Software Engineer</div>
  <div className={stylesdesk.slogan}>Specify. Test. Engineer. Automate.</div>
  </div>
  <div className={stylesdesk.below}>
  <div className={stylesdesk.designbutton}><WorkflowButton link='/UI.pdf' image="design.png"></WorkflowButton></div>
  <div className={stylesdesk.developbutton}><WorkflowButton link='/UX.pdf' image="dev.png"></WorkflowButton></div>
  <div className={stylesdesk.deploybutton}><WorkflowButton link='/FrontEnd.pdf' image="deploy.png"></WorkflowButton></div>
  </div>
</div>
</div>
  </>
  )
}
